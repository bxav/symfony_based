<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use ServiceMarketplace\Bundle\CoreBundle\Model\Solution as SolutionBase;

class Solution extends SolutionBase
{

    protected $id;

    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }
}
