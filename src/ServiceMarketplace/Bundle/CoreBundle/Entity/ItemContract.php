<?php

namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

class ItemContract
{
    protected $id;

    protected $endTime;

    protected $startTime;
    
    public function __construct(\DateTime $startTime, \DateInterval $interval)
    {
        $this->startTime = clone $startTime;
        $tempDate = clone $startTime;
        $this->endTime = $tempDate->add($interval);  
    }
    
    public function getId()
    {
        return $this->id;
    }
      
    public function getEndTime()
    {
        return $this->endTime;
    }

    public function getStartTime()
    {
        return $this->startTime;
        
    }
}
