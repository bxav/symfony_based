<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

class Order
{

    /**
     *
     * @var string
     */
    protected $number;

    protected $user;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var int
     */
    protected $totalAmount;

    /**
     *
     * @var string
     */
    protected $currencyCode;
    
    protected $status;
    
    protected $errorMessage;
    
    protected $details;

    const PENDING = 'pending';
    
    const COMPLETE = 'complete';
    
    const ERROR = 'error';
    
    const UNFINISHED = 'unfinished';
    
    public function __construct($number, User $user)
    {
        $this->status = self::UNFINISHED;
        $this->setNumber($number);
        $this->user = $user;
    }

    public function getNumber()
    {
        return $this->number;
    }

    protected function setNumber($number)
    {
        $this->number = $number;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     *
     * @param string $currencyCode            
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails(PaymentDetails $details)
    {
        $this->details = $details;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function complete()
    {
        $this->status = self::COMPLETE;
    }

    public function isComplete()
    {
        return $this->status === self::COMPLETE;
    }

    public function pending()
    {
        $this->status = self::PENDING;
    }

    public function isPending()
    {
        return $this->status === self::PENDING;
    }

    public function error($message = '')
    {
        $this->status = self::ERROR;
        $this->errorMessage = $message;
    }

    public function isError()
    {
        return $this->status === self::ERROR;
    }

    public function isUnfinished()
    {
        return $this->status === self::UNFINISHED;
    }
}
