<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use ServiceMarketplace\Bundle\CoreBundle\Model\Action as ActionBase;
use Bxav\Bundle\ServiceHandlerClientBundle\Entity\SoapAction;

class Action extends ActionBase
{

    protected $id;
    
    protected $soapAction;
    
    public function getId()
    {
        return $this->id;
    }

    public function setSoapAction(SoapAction $soapAction)
    {
        $this->soapAction = $soapAction;
    }

    public function getSoapAction()
    {
        return $this->soapAction;
    }
}
