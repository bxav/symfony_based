<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use ServiceMarketplace\Bundle\CoreBundle\Model\ActionConfig as ActionConfigBase;
use Doctrine\Common\Collections\ArrayCollection;

class ActionConfig extends ActionConfigBase
{

    protected $id;
    
    public function __construct() {
        $this->arguments = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }
}
