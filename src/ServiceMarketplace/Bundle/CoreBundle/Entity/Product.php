<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ServiceMarketplace\Bundle\CoreBundle\Model\Product as ProductBase;

class Product extends ProductBase
{
    
    protected $id;
    
    public function __construct() {
        $this->dependencies = new ArrayCollection();
        $this->actions = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getDependencies()
    {
        return $this->dependencies;
    }
    
    public function getActions()
    {
        return $this->actions;
    }
}
