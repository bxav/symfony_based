<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class ItemConfig
{

    protected $id;

    protected $actionConfigs;

    public function __construct()
    {
        $this->actionConfigs = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setActionConfigs($actionConfigs)
    {
        $this->actionConfigs = $actionConfigs;
    }

    public function getActionConfigs()
    {
        return $this->actionConfigs;
    }

    public function hasActionConfigs()
    {
        return ! $this->actionConfigs->isEmpty();
    }
}
