<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use ServiceMarketplace\Bundle\CoreBundle\Model\Item as ItemBase;

class Item extends ItemBase
{

    protected $id;

    protected $config;
    
    protected $status;
    
    protected $contract;
    
    const UNCONFIGURE = 'unconfigure';
    
    const READY = 'ready';
    
    const PROCESSING = 'processing';
    
    const ERROR = 'error';
    
    protected $errorMessage;
    
    public function __construct($product, $solution) {
        parent::__construct($product, $solution);
        $this->status = self::UNCONFIGURE;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getConfig()
    {
        return $this->config;
    }
    
    public function setConfig($config)
    {
        $this->config = $config;
    }
    
    public function getStatus()
    {
        return $this->status;
    }

    public function isUnconfigure()
    {
        return $this->status === self::UNCONFIGURE;
    }
    
    public function processing()
    {
        $this->status = self::PROCESSING;
    }
    
    public function isProcessing()
    {
        return $this->status === self::PROCESSING;
    }
    
    public function error($message)
    {
        $this->status = self::ERROR;
        $this->errorMessage = $message;
    }
    
    public function isError()
    {
        return $this->status === self::ERROR;
    }

    public function ready()
    {
        $this->status = self::READY;
    }

    public function isReady()
    {
        return $this->status === self::READY;
    }

    public function setContract($contract)
    {
        $this->contract = $contract;
    }

    public function getContract()
    {
        return $this->contract;
    }
}
