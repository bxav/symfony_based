<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use ServiceMarketplace\Bundle\CoreBundle\Model\ActionArgument as ActionArgumentBase;
use Doctrine\Common\Collections\ArrayCollection;

class ActionArgument extends ActionArgumentBase
{

    protected $id;
    
    public function __construct() {
        $this->arguments = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }
}
