<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use Payum\Core\Model\ArrayObject;

class PaymentDetails extends ArrayObject
{

    protected $id;
    
    public function getId()
    {
        return $this->id;
    }
}