<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ItemRepository extends EntityRepository
{
    public function findBySolutionAndStatus(Solution $solution, array $arrStatus = [Item::PROCESSING], $maxResults = null)
    {
        // TODO use doctrine query
        $queryBuilder = $this->createQueryBuilder('i')
            ->where('i.solution = :solution')
            ->setParameter('solution', $solution)
            ->andWhere('i.status IN (:status)')
            ->setParameter('status', $arrStatus);
        if (is_int($maxResults) && $maxResults >= 0) {
            $queryBuilder->setMaxResults($maxResults);
        }
        
        return $queryBuilder->getQuery()->getResult();
        
    }
}