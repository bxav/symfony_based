<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAllWithoutDependencies()
    {
        $products = $this->getEntityManager()->getRepository('ServiceMarketplaceCoreBundle:Product')->findAll();
        $productWithoutDep = [];
        foreach ($products as $product) {
            if (count($product->getDependencies()) == 0) {
                $productWithoutDep[] = $product;
            }
        }
        
        return $productWithoutDep;
    }
}