<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use ServiceMarketplace\Bundle\CoreBundle\Model\UserInterface;

class User extends BaseUser implements UserInterface
{

    protected $id;

    protected $credit = 0;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    
    public function hasCredit()
    {
        return $this->credit > 0;
    }

    public function addCredit($credit)
    {
        $this->isValidOrThrowExcection($credit);
        $this->credit += $credit; 
    }
    
    public function takeCredit($credit)
    {
        $this->isValidOrThrowExcection($credit);
        $this->credit -= $credit; 
    }
    
    public function getCredit()
    {
        return $this->credit;
    }
    
    public function isValidOrThrowExcection($credit)
    {
        if (!is_int($credit) || $credit < 0) {
            throw new \Exception("Bad argument");
        }
    }
}
