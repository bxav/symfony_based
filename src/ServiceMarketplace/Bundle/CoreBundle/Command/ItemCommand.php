<?php

namespace ServiceMarketplace\Bundle\CoreBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use ServiceMarketplace\Bundle\CoreBundle\Helper\ItemProcessor;
use Doctrine\ORM\EntityRepository;

class ItemCommand extends Command
{
    
    protected $itemRepository;
    
    protected $itemProcessor;
    
    protected $em;
    
    public function __construct(EntityManager $em, ItemProcessor $itemProcessor)
    {
        $this->itemRepository = $em->getRepository('ServiceMarketplaceCoreBundle:Item');
        $this->em = $em;
        $this->itemProcessor = $itemProcessor;
        parent::__construct();
    }
    
    protected function configure()
    {
        $this->setName('marketplace:item:process')
            ->setDescription('process an item')
            ->addArgument('id', InputArgument::REQUIRED, 'Which item do you want to process?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $item = $this->itemRepository->find($input->getArgument('id'));
        try {
            $this->itemProcessor->process($item);
            $item->finish();
        } catch (\Exception $e) {
            // TODO log real message error and sand standard message to user            
            $item->error($e);
        } finally {
            $this->em->persist($item);
            $this->em->flush();
        }
        
        
        $output->writeln('end');
    }
}