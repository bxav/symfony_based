<?php

namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class ActionConfig
{

    protected $arguments = [];
    
    protected $action;
    
    protected $prototype = true;

    public function setAction($action)
    {
        $this->action = $action;
    }
    
    public function getAction()
    {
        return $this->action;
    }
    
    public function setActionArguments($arguments)
    {
        $this->arguments = $arguments;
    }

    public function getActionArguments()
    {
        return $this->arguments;
    }
    
    public function hasActionArguments()
    {
        return ! empty($this->arguments);
    }
    
    public function __clone()
    {
        $newArr = [];
        foreach ($this->arguments as $argument) {
            $newArr[] = clone $argument;
        }
            
        $this->arguments = $newArr;
        $this->prototype = false;
    }
}
