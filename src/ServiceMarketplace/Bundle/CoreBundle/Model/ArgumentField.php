<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class ArgumentField
{

    const FIXED = 0;

    const USER = 1;

    const SYSTEM = 2;

    const ITEM = 3;

    const PRODUCT = 4;

    const FIXED_LABEL = 'fixed field';

    const USER_LABEL = 'user field';

    const SYSTEM_LABEL = 'system field';

    const ITEM_LABEL = 'item field';

    const PRODUCT_LABEL = 'product field';

    static public function getAllType()
    {
        return [
            self::FIXED => self::FIXED_LABEL,
            self::USER => self::USER_LABEL,
            self::SYSTEM => self::SYSTEM_LABEL,
            self::ITEM => self::ITEM_LABEL,
            self::PRODUCT => self::PRODUCT_LABEL
        ];
    }
}
