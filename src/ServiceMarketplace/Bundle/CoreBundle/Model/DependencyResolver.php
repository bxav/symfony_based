<?php

namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class DependencyResolver
{
    private $listProcessed = [];
    
    public function resolveAndProcess(Dependency $dep)
    {
        if ($dep->isProcessed())
            return $this->listProcessed;
        foreach ($dep->getDependencies() as $dependency) {
            $this->resolveAndProcess($dependency);
        }
        $dep->process();
        $this->listProcessed[] = $dep;
        return $this->listProcessed;
    }
}
