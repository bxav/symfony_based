<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class Item
{

    protected $product;

    protected $solution;

    public function __construct(Product $product, Solution $solution)
    {
        $this->product = $product;
        $this->solution = $solution;
    }
    
    public function getName()
    {
        return $this->product->getName();
    }
    
    public function getProduct()
    {
        return $this->product;
    }
    
    public function getSolution()
    {
        return $this->solution;
    }
    
    public function getActionsToProcess()
    {
        return $this->product->getActions();
    }
}
