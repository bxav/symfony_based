<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Model;

interface Dependency
{

    public function addDependency(Dependency $dependency);

    public function getDependencies();
    
    public function isProcessed();
    
    public function process();
}
