<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class Action
{
    
    protected $name;
    
    protected $config;
        
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function setActionConfig(ActionConfig $config)
    {
        $this->config = $config;
    }

    public function getActionConfig()
    {
        return $this->config;
    }
}
