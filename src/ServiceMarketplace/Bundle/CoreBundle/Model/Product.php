<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class Product implements Dependency
{

    protected $name;

    protected $dependencies = [];
    
    protected $dependants = [];

    protected $process = false;

    protected $actions = [];
    
    protected $price;
    
    protected $description;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function addDependency(Dependency $dependency)
    {
        $this->dependencies[] = $dependency;
        return $this;
    }

    public function getDependencies()
    {
        return $this->dependencies;
    }
    
    public function getDependants()
    {
        return $this->dependants;
    }
    
    public function hasDependencies()
    {
        return !($this->getDependencies() == null or $this->getDependencies() == []);
    }

    public function isProcessed()
    {
        return $this->process;
    }

    public function process()
    {
        $this->process = true;
    }

    public function hasActions()
    {
        return ! empty($this->actions);
    }

    public function addAction(Action $action)
    {
        $this->actions[] = $action;
    }
    
    public function getActions()
    {
        return $this->actions;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }
}
