<?php

namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class Solution
{

    protected $items = [];
    
    protected $name;
    
    protected $owner;
    
    public function getItems()
    {
        return $this->items;
    }

    public function addItem(Item $item)
    {
        $this->items[] = $item;
    }

    public function hasItem(Item $item)
    {
        return in_array($item, $this->getItems());
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setOwner(UserInterface $owner)
    {
        $this->owner = $owner;
    }

    public function getOwner()
    {
        return $this->owner;
    }
}
