<?php

namespace ServiceMarketplace\Bundle\CoreBundle\Model;

class ActionArgument
{
    
    protected $type;
    
    protected $value = '';
    
    protected $fieldType = ArgumentField::FIXED;
    
    protected $prototype = true;
    
    public function setFieldType($fieldType)
    {
        $this->fieldType = $fieldType;
    }

    public function getFieldType()
    {
        return $this->fieldType;
    }
    
    public function setType($type)
    {
        $this->type = $type;
    }
    
    public function getType()
    {
        return $this->type;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function __clone()
    {
        $this->prototype = false;
    }
}
