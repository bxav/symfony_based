<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Helper;

use ServiceMarketplace\Bundle\CoreBundle\Entity\Item;
use Bxav\Bundle\ServiceHandlerClientBundle\Model\ActionProcessor as SoapActionProcessor;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Action;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ActionConfig;

class ItemProcessor
{

    protected $actionProcessor;

    public function __construct(SoapActionProcessor $actionProcessor)
    {
        $this->actionProcessor = $actionProcessor;
    }

    public function process(Item $item)
    {
        $itemConfig = $item->getConfig();
        $actionConfigs = $itemConfig->getActionConfigs();
        foreach ($actionConfigs as $actionConfig) {
            $this->actionProcessor->process($this->getUpdateSoapAction($actionConfig));
        }
    }
    
    private function getUpdateSoapAction(ActionConfig $actionConfig) {
        $args = $actionConfig->getActionArguments();
        $soapArg = [];
        foreach ($args as $arg) {
            $soapArg[] = [ 'type' => $arg->getType(), 'value' => $arg->getValue()];
        }
        $soapAction = $actionConfig->getAction()->getSoapAction();
        $soapAction->setArguments($soapArg);
        return $soapAction;
    }
}
