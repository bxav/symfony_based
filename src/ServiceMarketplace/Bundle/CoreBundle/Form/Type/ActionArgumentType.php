<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ServiceMarketplace\Bundle\CoreBundle\Model\ArgumentField;

class ActionArgumentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fieldType', 'choice', [
                'choices' => ArgumentField::getAllType()
            ])
            ->add('type', 'text')
            ->add('value', 'text');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ServiceMarketplace\Bundle\CoreBundle\Entity\ActionArgument'
        ));
    }

    public function getName()
    {
        return 'action_argument';
    }
}