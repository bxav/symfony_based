<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActionConfigType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('actionArguments','collection', array('type' => new ActionArgumentType()));         

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ServiceMarketplace\Bundle\CoreBundle\Entity\ActionConfig'
        ));
    }

    public function getName()
    {
        return 'action_config';
    }
}