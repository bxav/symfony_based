<?php
namespace ServiceMarketplace\Bundle\CoreBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityRepository;
use ServiceMarketplace\Bundle\CoreBundle\Form\Type\ActionConfigType;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ActionConfig;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ActionArgument;

class AddActionArgumentFieldSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT   => 'preSubmit'
        );
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        if (null === $data) {
            return;
        }
        $accessor    = PropertyAccess::createPropertyAccessor();
        $soapAction = $accessor->getValue($data, 'soapAction');

        $soapActionArgs = ($soapAction) ? $soapAction->getArguments() : null;
        
        $config = $accessor->getValue($data, 'actionConfig');
        if ($config && $config->getActionArguments()->isEmpty()) {
            $config = $this->setField($soapActionArgs);
            $config->setAction($data);
            $accessor->setValue($data, 'actionConfig', $config);
        }
    }

    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
    }
    
    private function setField($arguments)
    {
        $actionConfig = new ActionConfig();
        
        $actionArgs = [];
        if (is_array($arguments)) {
        
            foreach ($arguments as $argument) {
                $actionArg = new ActionArgument();
                $actionArg->setType($argument['type']);
                $actionArg->setValue($argument['value']);
                $actionArgs[] = $actionArg;
            }
            
        }
        $actionConfig->setActionArguments($actionArgs);
        return $actionConfig;
    }
}