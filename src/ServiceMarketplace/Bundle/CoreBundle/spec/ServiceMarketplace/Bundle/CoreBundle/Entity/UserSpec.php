<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\User');
    }
    
    function it_extends_fos_user()
    {
        $this->shouldHaveType('FOS\UserBundle\Entity\User');
    }
    
    function it_should_not_have_credit_by_default()
    {
        $this->shouldNotHaveCredit();
    }

    function it_throws_exception_if_credit_arg_isnt_positive_int()
    {
        $this->shouldThrow('Exception')->duringAddCredit('not int');
        $this->shouldThrow('Exception')->duringAddCredit(10.3);
        $this->shouldThrow('Exception')->duringAddCredit(-10);
    }
    
    function it_should_add_credit()
    {
        $this->addCredit(10);
        $this->getCredit()->shouldReturn(10);
    }
    
    function it_should_take_credit()
    {
        $this->addCredit(20);
        $this->takeCredit(7);
        $this->getCredit()->shouldReturn(13);
    }
}
