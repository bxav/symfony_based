<?php

namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Bxav\Bundle\ServiceHandlerClientBundle\Entity\SoapAction;

class ActionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\Action');
    }
    
    function it_extends_item_model()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Action');
    }
    
    function its_soap_action_is_mutable(SoapAction $soapAction)
    {
        $this->setSoapAction($soapAction);
        $this->getSoapAction()->shouldReturn($soapAction);
    }
}
