<?php

namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\ActionConfig;

class ActionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Action');
    }
    
    function it_may_have_an_action_config(ActionConfig $config)
    {
        $this->setActionConfig($config);
        $this->getActionConfig()->shouldReturn($config);
    }
}
