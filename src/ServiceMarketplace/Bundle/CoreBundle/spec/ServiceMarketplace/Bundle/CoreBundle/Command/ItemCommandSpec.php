<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Command;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Helper\ItemProcessor;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;

class ItemCommandSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Command\ItemCommand');
    }
    
    function let(EntityManager $em, ItemProcessor $itemProcessor)
    {
        $this->beConstructedWith($em, $itemProcessor);
    }
}
