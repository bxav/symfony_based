<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ActionArgumentSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\ActionArgument');
    }
    
    function its_type_and_value_should_be_mutable($type, $value)
    {
        $this->setType($type);
        $this->getType()->shouldReturn($type);
        
        $this->setValue($value);
        $this->getValue()->shouldReturn($value);
    }
}
