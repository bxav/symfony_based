<?php

namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\Dependency;
use ServiceMarketplace\Bundle\CoreBundle\Model\Action;

class ProductSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Product');
    }
    
    function it_is_a_dependency()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Dependency');
    }
    
    function it_may_depend_from_other_product(Dependency $dep)
    {
        $this->addDependency($dep);
        $this->getDependencies()->shouldBeArray();
    }
    
    function it_is_not_processed_by_default()
    {
        $this->isProcessed()->shouldReturn(false);
    }
    
    function it_has_not_action_by_default()
    {
        $this->shouldNotHaveActions();
    }
    
    function it_should_add_action(Action $action)
    {
        $this->addAction($action);
        $this->shouldHaveActions();
    }
    
    function it_may_be_processed()
    {
        $this->process();
        $this->isProcessed()->shouldReturn(true);
    }
    
    function its_price_should_be_mutable($price)
    {
        $this->setPrice($price);
        $this->getPrice()->shouldReturn($price);
    }
    
    function its_description_should_be_mutable($description)
    {
        $this->setDescription($description);
        $this->getDescription()->shouldReturn($description);
    }
}
