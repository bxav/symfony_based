<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Entity\User;
use ServiceMarketplace\Bundle\CoreBundle\Entity\PaymentDetails;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Order;

class OrderSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\Order');
    }
    
    function let($number, User $user)
    {
        $this->beConstructedWith($number, $user);
    }
    
    function its_details_are_mutable(PaymentDetails $details)
    {
        $this->setDetails($details);
        $this->getDetails()->shouldReturn($details);
    }
    
    function it_should_change_is_status_to_complete()
    {
        $this->complete();
        $this->shouldBeComplete();
        $this->getStatus()->shouldReturn(Order::COMPLETE);
    }
    
    function it_should_change_is_status_to_pending()
    {
        $this->pending();
        $this->shouldBePending();
        $this->getStatus()->shouldReturn(Order::PENDING);
    }
    
    function it_should_be_unfinished_by_default()
    {
        $this->shouldBeUnfinished();
        $this->getStatus()->shouldReturn(Order::UNFINISHED);
    }
    
    function it_should_be_an_error_state()
    {
        $this->error("error");
        $this->shouldBeError();
        $this->getStatus()->shouldReturn(Order::ERROR);
    }
}
