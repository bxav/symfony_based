<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\ActionArgument;

class ActionConfigSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\ActionConfig');
    }
    
    function it_should_not_have_action_arguments_by_default()
    {
        $this->shouldNotHaveActionArguments();
    }
    
    function it_should_be_clonable()
    {
        clone $this;
    }

    function it_has_action_arguments(ActionArgument $arg1, ActionArgument $arg2)
    {
        $arrActinoArgument = [$arg1, $arg2];
        $this->setActionArguments($arrActinoArgument);
        $this->shouldHaveActionArguments();
    }
}
