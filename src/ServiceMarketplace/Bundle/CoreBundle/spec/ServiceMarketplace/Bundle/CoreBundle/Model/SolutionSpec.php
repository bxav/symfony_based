<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\Item;
use ServiceMarketplace\Bundle\CoreBundle\Model\UserInterface;

class SolutionSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Solution');
    }
    
    function it_should_not_have_item_by_default()
    {
        $this->getItems()->shouldReturn([]);
    }
    
    function it_should_be_able_to_add_new_item(Item $item)
    {
        $this->addItem($item);
        $this->shouldHaveItem($item);
    }
    
    function its_name_is_mutable($solutionName)
    {
        $this->setName($solutionName);
        $this->getName()->shouldReturn($solutionName);
    }
    
    function its_owner_is_mutable(UserInterface $owner)
    {
        $this->setOwner($owner);
        $this->getOwner()->shouldReturn($owner);
    }    
}
