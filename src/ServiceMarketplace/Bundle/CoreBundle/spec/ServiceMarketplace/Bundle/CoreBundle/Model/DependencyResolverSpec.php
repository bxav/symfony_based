<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\Product;

class DependencyResolverSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\DependencyResolver');
    }

    function it_should_resolve_dependencies()
    {
        $blog = new Product();
        $siteWeb = new Product();
        $siteWeb->process();
        $crm = new Product();
        $tracker = new Product();
        $tracker->addDependency($blog)->addDependency($crm);
        $blog->addDependency($siteWeb);
        
        $this->resolveAndProcess($tracker)->shouldReturn([
            $blog,
            $crm, $tracker]);
    }
}
