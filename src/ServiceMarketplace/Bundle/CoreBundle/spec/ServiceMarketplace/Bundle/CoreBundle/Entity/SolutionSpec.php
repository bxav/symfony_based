<?php

namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SolutionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\Solution');
    }
    
    function it_extends_solution_model()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Solution');
    }
}
