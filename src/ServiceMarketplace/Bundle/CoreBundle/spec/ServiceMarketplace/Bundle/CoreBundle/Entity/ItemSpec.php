<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\Product;
use ServiceMarketplace\Bundle\CoreBundle\Model\Solution;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Item;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ItemContract;

class ItemSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\Item');
    }

    function it_extends_item_model()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Item');
    }

    function let(Product $product, Solution $solution)
    {
        $this->beConstructedWith($product, $solution);
    }

    function it_should_be_unconfigure_by_default()
    {
        $this->shouldBeUnconfigure();
        $this->getStatus()->shouldReturn(Item::UNCONFIGURE);
    }
    
    function it_should_change_is_status_to_ready()
    {
        $this->ready();
        $this->shouldBeReady();
        $this->getStatus()->shouldReturn(Item::READY);
    }
    
    function it_should_change_is_status_to_processing()
    {
        $this->processing();
        $this->shouldBeProcessing();
        $this->getStatus()->shouldReturn(Item::PROCESSING);
    }
    
    function it_should_be_an_error_state()
    {
        $this->error("error");
        $this->shouldBeError();
        $this->getStatus()->shouldReturn(Item::ERROR);
    }
    
    function its_contract_is_mutable(ItemContract $contract)
    {
        $this->setContract($contract);
        $this->getContract()->shouldReturn($contract);
    }
}
