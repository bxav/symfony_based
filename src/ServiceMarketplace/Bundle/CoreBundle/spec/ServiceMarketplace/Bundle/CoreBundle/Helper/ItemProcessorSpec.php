<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Helper;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Item;
use Bxav\Bundle\ServiceHandlerClientBundle\Model\ActionProcessor as SoapActionProcessor;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ItemConfig;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ActionConfig;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ActionArgument;
use Bxav\Bundle\ServiceHandlerClientBundle\Entity\SoapAction;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Action;

class ItemProcessorSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Helper\ItemProcessor');
    }
    
    function let(SoapActionProcessor $actionProcessor)
    {
        $this->beConstructedWith($actionProcessor);
    }
    
    function it_should_process_an_item(
        Item $item,
        ItemConfig $config,
        ActionConfig $actionConfig,
        ActionArgument $actArg,
        Action $action,
        SoapAction $soapAction
    ) {
        
        // TODO noodle, something is in, extract new service 
        $action->getSoapAction()->willReturn($soapAction);
        $actionConfig->getActionArguments()->willReturn([$actArg, clone $actArg]);
        $actionConfig->getAction()->willReturn($action);
        $config->getActionConfigs()->willReturn([$actionConfig]);
        $item->getConfig()->willReturn($config);
        
        $this->process($item);
    }
}
