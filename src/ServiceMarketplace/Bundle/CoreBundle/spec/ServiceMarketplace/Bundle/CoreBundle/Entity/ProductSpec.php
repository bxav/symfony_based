<?php

namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProductSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\Product');
    }
    
    function it_extends_product_model()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Product');
    }
}
