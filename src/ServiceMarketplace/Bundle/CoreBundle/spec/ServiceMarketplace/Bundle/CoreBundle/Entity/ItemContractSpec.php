<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Entity;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ItemContractSpec extends ObjectBehavior
{

    protected $startTime;
    
    protected $interval;
    
    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Entity\ItemContract');
    }

    function let()
    {
        $startTime = new \DateTime('now');
        $interval = new \DateInterval('P1M');
        $this->beConstructedWith($startTime, $interval);
        $this->startTime = clone $startTime;
        $this->interval = $interval;
    }
    
    function it_should_have_end_time_by_default()
    {
        $endTime = $this->startTime->add($this->interval);
        $this->getEndTime()->shouldBeLike($endTime);    
    }
    
    function it_should_have_start_time_by_default()
    {
        $this->getStartTime()->shouldBeLike($this->startTime);
    }
}
