<?php
namespace spec\ServiceMarketplace\Bundle\CoreBundle\Model;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use ServiceMarketplace\Bundle\CoreBundle\Model\Product;
use ServiceMarketplace\Bundle\CoreBundle\Model\Solution;

class ItemSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType('ServiceMarketplace\Bundle\CoreBundle\Model\Item');
    }

    function let(Product $product, Solution $solution)
    {
        $this->beConstructedWith($product, $solution);
    }
}
