<?php
namespace ServiceMarketplace\Bundle\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductAdmin extends Admin
{
    
    protected $baseRoutePattern = 'product';
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('price', 'text', [
                'required' => false
            ])
            ->add('description', 'text')
            ->add('dependencies', 'entity', [
                'class' => 'ServiceMarketplaceCoreBundle:Product',
                'property' => 'name',
                'required' => false,
                'multiple' => true
            ])
            ->add('actions', 'entity', [
                'class' => 'ServiceMarketplaceCoreBundle:Action',
                'property' => 'name',
                'required' => false,
                'multiple' => true
            ]);
    }
    
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('price');
    }
    
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('price');
    }
}