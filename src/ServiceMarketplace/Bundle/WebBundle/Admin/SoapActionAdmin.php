<?php
namespace ServiceMarketplace\Bundle\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SoapActionAdmin extends Admin
{

    protected $baseRoutePattern = 'soap_action';

    protected function configureRoutes(RouteCollection $collection)
    {

        $collection->remove('create');
        $collection->remove('edit');
        //$collection->remove('delete');
        $collection->add('sync', 'sync');
    }
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', 'text', [
            'label' => $this->trans('marketplace.action.entity_label')
        ]);
    }
    
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('methodName');
    }
    
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('methodName')
                   ->add('service.wsdl');
    }
}