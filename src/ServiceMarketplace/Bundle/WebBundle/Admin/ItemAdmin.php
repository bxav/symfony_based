<?php
namespace ServiceMarketplace\Bundle\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ItemAdmin extends Admin
{
    
    protected $baseRoutePattern = 'item';
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
    }
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('product', 'entity', [
                'class' => 'ServiceMarketplaceCoreBundle:Product',
                'property' => 'name'
            ])
            ->add('actions', 'entity', [
                'class' => 'ServiceMarketplaceCoreBundle:Solution',
                'property' => 'name'
            ]);
    }
    
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')->add('product.name');
    }
    
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')->add('product.name')->add('solution.name');
    }
}