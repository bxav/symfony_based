<?php
namespace ServiceMarketplace\Bundle\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class UserAdmin extends Admin
{
    
    protected $baseRoutePattern = 'user';
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('username')
                ->add('email')
                ->add('credit', 'integer', ['required' => false, 'mapped' => false, 'data' => 0])
                ->add('plainPassword', 'text', ['required' => false])
                ->add('enabled', null, ['required' => false])
                ->end();
        
        $formMapper->getFormBuilder()->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            $user = $form->getData();
            $credit = (int)$data['credit'];
            if ($credit >= 0) {
                $user->addCredit($credit);
            } else {
                $user->takeCredit((0 - $credit));
            }
        });
    }
    
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('enabled')
            ->add('email');
    }
    
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, ['route' => ['name' => 'show']])
            ->add('email')
            ->add('credit')
            ->add('enabled', null, array('editable' => true))
            ->add('createdAt');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General')
                ->add('username')
                ->add('email')
                ->end();
    }
}