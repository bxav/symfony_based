<?php
namespace ServiceMarketplace\Bundle\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ServiceMarketplace\Bundle\CoreBundle\Form\EventListener\AddActionArgumentFieldSubscriber;

use ServiceMarketplace\Bundle\CoreBundle\Form\Type\ActionConfigType;

class ActionAdmin extends Admin
{
    
    protected $baseRoutePattern = 'action';
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        
        $formMapper
            ->add('name', 'text', [
                'label' => $this->trans('marketplace.action.entity_label')
            ])            
            ->add('soapAction', 'entity', [
                'class' => 'BxavServiceHandlerClientBundle:SoapAction',
                'property' => 'methodName',
                'required' => true
            ])
        ;
        $formMapper->getFormBuilder()->add('actionConfig' , new ActionConfigType())->addEventSubscriber(new AddActionArgumentFieldSubscriber());
    }
    
    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }
    
    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
}