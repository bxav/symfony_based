<?php

namespace ServiceMarketplace\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Payum\Core\Request\GetHumanStatus;
use Symfony\Component\HttpFoundation\Request;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Order;

class CreditController extends Controller
{
    public function checkoutAction()
    {
        return $this->redirect($this->generateUrl('service_marketplace_web_solution'));
    }
    
    public function preparePaypalExpressCheckoutPaymentAction(Request $request)
    {
               
        $form = $this->createFormBuilder()
            ->add('amount', 'integer', ['label' => false])
            ->getForm();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            
            $order = new Order(uniqid(), $this->getUser());
            
            $paymentName = 'buy_credit_by_paypal';
        
            $storage = $this->get('payum')->getStorage('ServiceMarketplace\Bundle\CoreBundle\Entity\PaymentDetails');
            
            $data = $form->getData();
            
            /** @var \Acme\PaymentBundle\Entity\PaymentDetails $details */
            $details = $storage->createModel();
            $details['PAYMENTREQUEST_0_CURRENCYCODE'] = 'EUR';
            $details['PAYMENTREQUEST_0_AMT'] = $data['amount'];
            $details['SOLUTIONTYPE'] = 'Sole';
            $details['LANDINGPAGE'] = 'Billing';
            $details['TOTALTYPE'] = 'Total';
            $details['PAYMENTREQUEST_0_ITEMAMT'] = $data['amount'];
            $details['L_PAYMENTREQUEST_0_NAME0'] = ((int)$data['amount']) . " credits";
            $details['L_PAYMENTREQUEST_0_AMT0'] = $data['amount'];
            
            
            $storage->updateModel($details);
            
            $order->setTotalAmount((int)$details['PAYMENTREQUEST_0_AMT']);
            $order->setCurrencyCode($details['PAYMENTREQUEST_0_CURRENCYCODE']);
            $order->setDescription($details['L_PAYMENTREQUEST_0_NAME0']);
            $order->setDetails($details);
            
            $em->persist($order);
            $em->flush($order);
            
            $captureToken = $this->get('payum.security.token_factory')->createCaptureToken(
                $paymentName,
                $details,
                'service_marketplace_credit_capture_done' // the route to redirect after capture;
            );
        
            $details['INVNUM'] = $details->getId();
            $details['RETURNURL'] = $captureToken->getTargetUrl();
            $details['CANCELURL'] = $captureToken->getTargetUrl();
            $storage->updateModel($details);
        
            return $this->redirect($captureToken->getTargetUrl());
        }
        return $this->render('ServiceMarketplaceWebBundle:credit:buy.html.twig', array(
            'form' => $form->createView(),
        ));

    }
    
    public function captureDoneAction(Request $request)
    {
        $token = $this->get('payum.security.http_request_verifier')->verify($request);
        
        $repo = $this->getDoctrine()->getRepository('ServiceMarketplaceCoreBundle:Order');
        
        $payment = $this->get('payum')->getPayment($token->getPaymentName());
        $payment->execute($status = new GetHumanStatus($token));
        

        $data = $status->getModel();

        $order = $repo->findOneByDetails($data);
        
        if ($status->isSuccess()) {
            $user = $this->getUser();
            
            if ($data['PAYMENTREQUEST_0_CURRENCYCODE'] == 'EUR') {
                $user->addCredit((int)$data['PAYMENTREQUEST_0_AMT']);
                $order->complete();

                $this->getDoctrine()->getManager()->persist($user);
                $this->get('session')->getFlashBag()->add('notice', 'Payment success. Credits were added');
            } else {
                $order->pending();
                $this->get('session')->getFlashBag()->add('notice', 'Payment is still pending. Credits were not added, due to a currency problem');
            }
        } else if ($status->isPending()) {
            $order->pending();
            $this->get('session')->getFlashBag()->add('notice', 'Payment is still pending. Credits were not added');
        } else {
            $order->error();
            $this->get('session')->getFlashBag()->add('notice', 'Payment failed');
        }
        

        $this->getDoctrine()->getManager()->persist($order);
        $this->getDoctrine()->getManager()->flush();
        
    
        return $this->render('ServiceMarketplaceWebBundle:credit:invoice.html.twig');
    }
    
    public function widgetCreditAction()
    {
        return $this->render('ServiceMarketplaceWebBundle:credit:widget_navigation.html.twig', []);
    }
}
