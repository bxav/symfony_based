<?php
namespace ServiceMarketplace\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Item;
use JMS\JobQueueBundle\Entity\Job;
use ServiceMarketplace\Bundle\CoreBundle\Form\Type\ActionConfigType;
use ServiceMarketplace\Bundle\CoreBundle\Form\Type\ItemConfigType;
use Symfony\Component\HttpFoundation\Request;
use ServiceMarketplace\Bundle\CoreBundle\Model\ActionConfig;
use ServiceMarketplace\Bundle\CoreBundle\Model\ArgumentField;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ItemConfig;
use ServiceMarketplace\Bundle\CoreBundle\Form\Type\ItemFieldType;
use ServiceMarketplace\Bundle\CoreBundle\Model\ActionArgument;

class SolutionController extends Controller
{

    public function indexAction()
    {
        $solution = $this->getDoctrine()
            ->getRepository('ServiceMarketplaceCoreBundle:Solution')
            ->findOneByOwner($this->getUser());
        
        $items = $this->getDoctrine()
            ->getRepository('ServiceMarketplaceCoreBundle:Item')
            ->findBySolution($solution);
        
        return $this->render('ServiceMarketplaceWebBundle:solution:index.html.twig', [
            'items' => $items
        ]);
    }

    public function processAction(Item $item)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $job = new Job('marketplace:item:process', [$item->getId()]);
        $item->processing();
        $em->persist($item);
        $em->persist($job);
        $em->flush();
        
        return $this->redirect($this->generateUrl('service_marketplace_web_solution'));
    }
    
    public function listProcessingAndCompleteItemAction()
    {
        $items = $this->getItemByStatus([Item::PROCESSING, Item::ERROR]);
    
        return $this->render('ServiceMarketplaceWebBundle:task:index.html.twig', [
            'items' => $items
        ]);
    }
    
    public function widgetListProcessingAndCompleteItemAction($limit)
    {
        $items = $this->getItemByStatus([Item::PROCESSING, Item::ERROR], $limit);
    
        return $this->render('ServiceMarketplaceWebBundle:task:widget_navigation.html.twig', [
            'items' => $items
        ]);
    }

    public function configItemAction(Request $request, Item $item)
    {
        $itemActionArguments = $this->getItemActionArgumentFromItem($item);
        
        $form = $this->createItemFieldFormFromActionArguments($itemActionArguments);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $arrActionArgument = $form->getData();
            $this->persistAndFlushActionArguments($arrActionArgument);
            $this->persistReadyItem($item);
            
            
            return $this->redirect($this->generateUrl('service_marketplace_web_solution'));
        }
        
        return $this->render('ServiceMarketplaceWebBundle:solution:item_config.html.twig', [
            'form' => $form->createView()
        ]);
    }

    protected function getItemByStatus(array $status, $limit = null)
    {
        $solution = $this->getDoctrine()
                         ->getRepository('ServiceMarketplaceCoreBundle:Solution')
                         ->findOneByOwner($this->getUser());
        
        return $this->getDoctrine()
                    ->getRepository('ServiceMarketplaceCoreBundle:Item')
                    ->findBySolutionAndStatus($solution, $status, $limit);
    }
    
    protected function persistAndFlushActionArguments($arrActionArgument)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        foreach ($arrActionArgument->fields as $arg) {
            if ($arg->getFieldType() == ArgumentField::ITEM) {
                $em->persist($arg);
            }
        }
        $em->flush();
    }

    protected function createItemFieldFormFromActionArguments(array $arg)
    {
        $objToMap = new \stdClass();
        $objToMap->fields = $arg;
        $form = $this->createFormBuilder($objToMap)
            ->add('fields', 'collection', [
            'label' => false,
            'type' => new ItemFieldType()
        ])
            ->add('save', 'submit')
            ->getForm();
        return $form;
    }

    protected function getItemActionArgumentFromItem(Item $item)
    {
        $config = $item->getConfig();
        $argToKeep = [];
        foreach ($config->getActionConfigs() as $ac) {
            foreach ($ac->getActionArguments() as $arg) {
                if ($arg->getFieldType() == ArgumentField::ITEM) {     
                    $argToKeep[$arg->getType()] = $arg;
                }
            }
        }
        return $argToKeep;
    }
    
    protected function persistReadyItem($item)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $item->ready();
        $em->persist($item);
        $em->flush();
    }
}
