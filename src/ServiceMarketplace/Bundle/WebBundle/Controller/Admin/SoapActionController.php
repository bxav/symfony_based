<?php

namespace ServiceMarketplace\Bundle\WebBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SoapActionController extends Controller
{
    public function syncAction()
    {   
        ini_set("soap.wsdl_cache", "0");
        ini_set("soap.wsdl_cache_enabled", "0");
        ini_set('soap.wsdl_cache_ttl',0);
        
        
        $this->get('bxav_service_handler_client.service_synchronizer')->synchronize();

        $this->addFlash('sonata_flash_success', 'Sync successfully');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}