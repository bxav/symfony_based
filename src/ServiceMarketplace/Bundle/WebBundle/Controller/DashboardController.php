<?php
namespace ServiceMarketplace\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{

    public function indexAction()
    {
        $solution = $this->getDoctrine()
            ->getRepository('ServiceMarketplaceCoreBundle:Solution')
            ->findOneByOwner($this->getUser());
        $repo = $this->getDoctrine()->getRepository('ServiceMarketplaceCoreBundle:Order');
        $orders = $repo->findByUser($this->getUser());
        $repo = $this->getDoctrine()->getRepository('ServiceMarketplaceCoreBundle:Item');
        $items = $repo->findBySolution($solution);
        
        return $this->render('ServiceMarketplaceWebBundle:dashboard:dashboard.html.twig', [
            'orders' => $orders,
            'items' => $items
        ]);
    }
}
