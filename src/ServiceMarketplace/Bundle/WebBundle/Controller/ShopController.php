<?php

namespace ServiceMarketplace\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Product;
use ServiceMarketplace\Bundle\CoreBundle\Entity\Item;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ItemConfig;
use ServiceMarketplace\Bundle\CoreBundle\Entity\ItemContract;

class ShopController extends Controller
{    
    public function indexAction()
    {   
        $repo = $this->getDoctrine()->getRepository('ServiceMarketplaceCoreBundle:Product');
        $products = $repo->findAllWithoutDependencies();
        return $this->render('ServiceMarketplaceWebBundle:shop:index_products.html.twig', ['products' => $products]);
    }
    
    public function productAction(Product $product)
    {
        return $this->render('ServiceMarketplaceWebBundle:shop:product_view.html.twig', ['product' => $product]);
    }
    
    public function buyAction(Product $product)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $solution = $this->getDoctrine()->getRepository('ServiceMarketplaceCoreBundle:Solution')->findOneByOwner($this->getUser());
        $config = [];
        foreach ($product->getActions() as $action) {
            $protoConfig = $action->getActionConfig();
            $config[] = clone $protoConfig; 
        }
        $itemConfig = new ItemConfig();
        $itemConfig->setActionConfigs($config);
        $itemContract = new ItemContract(new \DateTime('now'), new \DateInterval('P1M'));
        
        $item = new Item($product, $solution);
        $user->takeCredit((int)$product->getPrice());
        $item->setConfig($itemConfig);
        $item->setContract($itemContract);
        
        $em->persist($item);
        $em->flush();
        return $this->redirect($this->generateUrl('service_marketplace_web_solution'));
    }
}
